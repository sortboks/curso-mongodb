db.articulos.insertOne({
   'nombre': 'tornillo philps 5',
   'codigo': 'tp5',
   'proveedor': 'burko',
   'material': 'acero',
   'costo': 10,
    'precio': 20,
    'cantidad': 50
});

db.articulos.insertOne({
    'nombre': 'Tornillo hexagonal 3',
    'codigo': 'th3',
    'proveedor': 'gata',
    'material': 'bronce',
    'costo': 18,
     'precio': 36,
     'cantidad': 70
 });

 db.articulos.insertOne({
    'nombre': 'Gruesa tuerca 3/4',
    'codigo': 'gt3/4',
    'proveedor': 'easy',
    'material': 'acero',
    'costo': 12,
     'precio':24,
     'cantidad': 100
 });

 db.articulos.insertOne({
    'nombre': 'pinza punta 6',
    'codigo': 'pp6',
    'proveedor': 'ferreoeste',
    'material': 'cromo',
    'costo': 100,
     'precio':200,
     'cantidad': 30
 });

 db.articulos.insertOne({
    'nombre': 'martillo bola 4',
    'codigo': 'mb4',
    'proveedor': 'ferr0sur',
    'material': 'madera acero',
    'costo': 23,
     'precio':46,
     'cantidad': 12
 });

 db.articulos.insertOne({
    'nombre': 'alambre 1 mm',
    'codigo': 'al1m',
    'proveedor': 'distribuidora norte',
    'material': 'acero galvanizado',
    'costo': 10,
     'precio':20,
     'cantidad': 25
 });

 // agregar atributo unidad

 db.articulos.updateMany(
     {},
     {$set: {'unidad': 'generico'}}
     );

db.articulos.updateOne(
    {
        codigo: 'tp5'
    },
    {
        $push: {
            movimiento: {
                cantidad: -3,
                fecha: new Date(),
                tipo: 'venta'
            }
        },
        $inc: {
            cantidad: -3
         
        }
    }
);

db.articulos.updateOne(
    {
        codigo: 'tp5'
    },
    {
        $set:{
            cantidad: 50
        },
        $unset:{
             movimiento: true  
        }
        
    }
);


db.movimientos.insertOne({
    'codigoarticulo':'tp5',
    'cantidad':-5,
    'tipo':'venta',
    'fecha': new Date()
});

db.movimientos.insertOne({
    'codigoarticulo':'th3',
    'cantidad':-10,
    'tipo':'venta',
    'fecha': new Date()
});

db.movimientos.insertOne({
    'codigoarticulo':'th3',
    'cantidad':144,
    'tipo':'compra',
    'fecha': new Date(),
    'proveedor': 'burko'
});

db.movimientos.insertOne({
    'codigoarticulo':'mb4',
    'cantidad':12,
    'tipo':'compra',
    'fecha': new Date(),
    'proveedor': 'easy'
});


db.articulos.aggregate(
    [
        {
            $lookup:{
                from:'movimientos',
                localField:'codigo',
                foreignField:'codigoarticulo',
                as:'movimientos'
            }
        }
    ]
);


db.movimientos.insertOne({
    'codigoarticulo':'mb4',
    'cantidad':1,
    'tipo':'venta',
    'fecha': new Date()
});

// Lista de precio
db.articulos.aggregate(
    [
        {
            $lookup:{
                from:'movimientos',
                localField:'codigo',
                foreignField:'codigoarticulo',
                as:'movimientos'
            }
        },
        {
            $project:{
                _id:0,
                codigo:1,
                nombre:1,
                precio:1
            }
        }
    ]
);

// lista de precio con financiacion 
db.articulos.aggregate(
    [
        {
            $lookup:{
                from:'movimientos',
                localField:'codigo',
                foreignField:'codigoarticulo',
                as:'movimientos'
            }
        },
        {
            $project:{
                _id:0,
                codigo:1,
                nombre:1,
                precio:{$multiply:['$precio',1.10]}
            }
        }
    ]
);

db.articulos.aggregate(
    [
        {
            $lookup:{
                from:'movimientos',
                localField:'codigo',
                foreignField:'codigoarticulo',
                as:'movimientos'
            }
        },
        {
            $group:{
                _id:{
                    tipo:'$movimientos.tipo',
                    codigo:'$codigo'},
                cantidad:{$sum:1}
            }
        }
    ]
);




