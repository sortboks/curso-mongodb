// Agregar la lat y long

db.users.updateOne(
  {
    username: 'user13'
  },
  {
    $set: {
      'address.location':{
        lat: -34,
        long: -57
      }
    }
  }
);

// Usuarios que tiene al menos una critica positiva
db.users.find(
  {
    comments: {
      $elemMatch: {
        like: true
      }
    }
  }
);

// Usuarios que tiene al menos una critica negativa
db.users.find(
  {
    comments: {
      $elemMatch: {
        like: false
      }
    }
  }
);

// Quiero ver los tags de criticas positivas
db.users.find(
  {
    comments: {
      $elemMatch: {
        $and: [
          { like: true},
          { tags: { $exists: true}}
        ]
      }
    }
  },
  {
    'comments.tags': 1
  }
);

// agregar un nuevo comentario positivo en usuario 31

db.users.updateOne(
  {
    username: 'user31'
  },
  {
    $push: {
      comments: {
        body: 'El profe es lo mas',
        like: true,
        tags: ['MongoDB']
      }
    }
  }
);

// agregar un curso nuevo
db.users.updateOne(
  {
    username: 'user31'
  },
  {
    $push: {
      courses: 'Rust'
    }
  }
);


// Agregar un nuevo tag al 4 comentario

db.users.updateOne(
  {
    username: 'user31'
  },
  {
    $push: {
      'comments.3.tags': 'Tutor'
    }
  }
);

// Agregar un nuevo tag al 2 comentario

db.users.updateOne(
  {
    username: 'user31'
  },
  {
    $push: {
      'comments.1.tags': 'Tutor'
    }
  }
);

// Modificar el segundo comentario user31

db.users.updateOne(
  {
    username: 'user31'
  },
  {
    $set: {
      'comments.1.body': 'Tengo la sarten por el mango!!'
    }
  }
);

// Actualizar los comentarios negativos del usuario 31

db.users.updateOne(
  {
    username: 'user31',
    'comments.like': false
  },
  {
    $set: {
      'comments.$.body': 'El curso me gusta mucho!!',
      'comments.$.like': true
    },
    $unset: {
      'comments.$.tags': true
    }
  }
);
