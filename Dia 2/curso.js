.editor

load("datos.js");

// Ordenados de Mayor a Menor
db.users.find().sort(
    {
        age: -1
    }
);

// El mayor de todos
db.users.find().sort(
    {
        age: -1
    }
).limit(1);

// Los tres mas grandes de todos
db.users.find().sort(
    {
        age: -1
    }
).limit(3);

// Del total de usuarios me muetra solo los 3 primeros
db.users.find().limit(3);

// Los tres ultimos usuarios incorporados
db.users.find().sort(
    {
        _id: -1
    }
).limit(3);


db.users.findOne();

// asignacion de una busqueda a una variable
var varx = db.users.find().sort(
    {
        age: -1
    }
);

varx;

// Expresiones regulares

db.users.find({
    username: /user1/
});

db.users.find({
    email: /.com$/
});

db.users.find({
    username: /ˆuser1/
});

// Metodos del cursor

db.users.find().limit(5);

db.users.find().skip(5);

db.users.find().skip(5).limit(5);

db.users.find().skip(10).limit(5);

db.users.find().skip(15).limit(5);

db.users.find().sort({
    age: -1
}).skip(2).limit(3);

db.users.find().sort({
    age: -1
}).limit(2);

db.users.find().sort({
    age: -1
}).skip(2).limit(3).pretty();

// Funciones

db.users.find().forEach(
    usuario => print(usuario.username, usuario.age)
);

// Proyeccion 

db.users.find(
    {},
    {
        username: true,
        age: true
    }
);

db.users.find(
    {},
    {
        _id: false,
        username: true,
        age: true
    }
);


db.users.find(
    {},
    {
        _id: 0,
        username: 1,
        age: 1
    }
);


db.users.find(
    {
        age: { $gte: 50}
    },
    {
        _id: 0,
        username: 1,
        age: 1
    }
);


db.users.find(
    {
        age: { $gte: 50}
    },
    {
        _id: 0,
        username: 1,
        age: 1
    }
).sort(
    { age: -1 }
);


// Modificar un documento

db.users.updateOne(
    {
        _id: ObjectId("614a2158f50d25856ba8c928")
    },
    {
        $set: {
            age: 37,
            email: 'popeye@mail.com',
            status: 'active'
        }
    }
);

db.users.updateOne(
    {
        _id: ObjectId("614a2158f50d25856ba8c928")
    },
    {
        $set: {
            picture: 'popeye.jpg'
        }
    }
);


db.users.updateOne(
    {
        _id: ObjectId("614a2158f50d25856ba8c928")
    },
    {
        $unset: {
            picture: true
        }
    }
);

db.users.updateOne(
    {
        _id: ObjectId("614a2158f50d25856ba8c928")
    },
    {
        $unset: {
            picture: 1
        }
    }
);


// Modificar mas de uno ó mas documentos

db.users.updateMany({
        status: 'inactive'
    },
    {
        $set: {
            status: 'active'
        }
    }
);

db.users.updateMany({
        email: { $exists: true}
    },
    {
        $set: {
            bio: 'Edita tu biografia',
            like: true
    }
});

db.users.updateMany({
    age: { $exists: true}
},
{
    $inc: {
        age: 1
    }
}
);

// borrar documentos

db.users.deleteOne({
    username: 'user2'
});

db.users.deleteMany({
    status: 'inactive'
});

// Borrar una coleccion

db.users.drop();
db.comments.drop();


// Borrar la base de datos donde estoy trabajando

db.dropDatabase();

// Documentos complejos

user31 = {
    'username': 'user31',
    'email': 'user31@mail.com',
    'age': 29,
    'status': 'active',
    'address': {
      'zip': 1001,
      'country': 'AR'
    },
    'courses': ['Linux','KVM','Docker','Kubernetes'],
    'comments': [
      {
        'body': 'El mejor curso',
        'like': true,
        'tags': ['MongoDB']
      },
      {
        'body': 'Muy emocionado',
        'like': true
      },
      {
        'body': 'El curso esta OK'
      },
      {
        'body': 'Malos cursos, estoy muy disconforme',
        'like': false,
        'tags': ['malo','curso','MongoDB']
      }
    ]
  };
user32 = {
    'username': 'user32',
    'email': 'user32@mail.com',
    'age': 31,
    'status': 'active',
    'comments': [
      {
        'body': 'El mejor curso',
        'like': true,
      }
    ]
  };
user33 = {
    'username': 'user33',
    'email': 'user33@mail.com',
    'age': 27,
    'status': 'active',
    'comments': [
      {
        'body': 'El mejor curso',
        'like': false,
      }
    ]
  };

db.users.insertMany([user31, user32, user33]);

db.users.find(
    {
        'address.country': 'AR'
    },
    {
        _id: 0,
        username: 1,
        'address.zip':1
    }
);

db.users.updateMany(
    {
        'address.zip': { $exists: true}
    },
    {
        $set: {
            'address.zip': 1002
        }
    }
);


db.users.updateMany(
    {
        'address.zip': { $exists: true},
        'address.zip': 1002
    },
    {
        $set: {
            'address.city': 'CABA'
        }
    }
);

db.users.find(
    { 
        'address.country': 'AR' 
    },
    {
        _id: 0,
        username: 1,
        'address.city': 1
    }
);

db.users.updateMany(
    {
        'address': { $exists: false}
    },
    {
        $set: {
            'address': {
                'country': 'AR',
                'city': 'La Plata',
                'zip': 1900
            }
        }
    }
);

db.users.find(
    { 
        'address.country': 'AR' 
    },
    { 
        _id: 0, 
        username: 1, 
        'address.city': 1, 
        'address.zip': 1 
    }
);

db.users.find(
    {
        courses: { $exists: true}
    },
    {
        _id: 0,
        username: 1,
        courses: 1
    }
);
