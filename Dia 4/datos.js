db.empleados.insertOne({nombre:'Ariel', salario:1000, sexo:'M',dias:7,area:'Cosecha', edad:30, fechapago:ISODate('2021-01-01')});
db.empleados.insertOne({nombre:'Juana', salario:1500, sexo:'F',dias:6,area:'Seleccion', edad:45, fechapago:ISODate('2021-01-15')});
db.empleados.insertOne({nombre:'Miguel', salario:1000, sexo:'M',dias:6,area:'Cosecha', edad:20, fechapago:ISODate('2021-01-01')});
db.empleados.insertOne({nombre:'Lidia', salario:1500, sexo:'F',dias:7,area:'Seleccion', edad:45, fechapago:ISODate('2021-01-15')});
db.empleados.insertOne({nombre:'Esteban', salario:1000, sexo:'M',dias:6,area:'Seleccion', edad:20, fechapago:ISODate('2021-01-01')});
db.empleados.insertOne({nombre:'Pedro', salario:1000, sexo:'M',dias:5,area:'Cosecha', edad:25, fechapago:ISODate('2021-01-15')});
db.empleados.insertOne({nombre:'Laura', salario:2000, sexo:'F',dias:7,area:'Calidad', edad:50, fechapago:ISODate('2021-02-01')});
db.empleados.insertOne({nombre:'Jose', salario:1000, sexo:'M',dias:7,area:'Cosecha', edad:30, fechapago:ISODate('2021-02-15')});
db.empleados.insertOne({nombre:'Claudia', salario:2000, sexo:'F',dias:6,area:'Calidad', edad:45, fechapago:ISODate('2021-02-01')});
db.empleados.insertOne({nombre:'Alejandro', salario:1500, sexo:'M',dias:6,area:'Empaque', edad:22, fechapago:ISODate('2021-02-15')});
db.empleados.insertOne({nombre:'Luciano', salario:1500, sexo:'M',dias:6,area:'Empaque', edad:22, fechapago:ISODate('2021-02-15')});
db.empleados.insertOne({nombre:'Raul', salario:1500, sexo:'M',dias:6,area:'Empaque', edad:22, fechapago:ISODate('2021-02-15')});
db.empleados.insertOne({nombre:'Brian', salario:1000, sexo:'M',dias:5,area:'Cosecha', edad:27, fechapago:ISODate('2021-03-01')});
db.empleados.insertOne({nombre:'Jesica', salario:1500, sexo:'F',dias:7,area:'Seleccion', edad:46, fechapago:ISODate('2021-03-15')});
db.empleados.insertOne({nombre:'Walter', salario:1000, sexo:'M',dias:6,area:'Cosecha', edad:31, fechapago:ISODate('2021-03-01')});
db.empleados.insertOne({nombre:'Sandra', salario:2000, sexo:'F',dias:6,area:'Calidad', edad:43, fechapago:ISODate('2021-03-05')});

db.cursada.insert({'_id':1, 'nombrec':'S101', carrera: 'Sistemas'});
db.cursada.insert({'_id':2, 'nombrec':'H101', carrera: 'Sistemas'});
db.cursada.insert({'_id':3, 'nombrec':'I101', carrera: 'Economicas'});

db.alumnos.insert({'_id': 1, 'matricula':'22334455', 'nombre':'Juan', 'apellido': 'Lopez', 'sexo': 'M', 'idc':1, 'edad': 20})
db.alumnos.insert({'_id': 2, 'matricula':'27324485', 'nombre':'Jose', 'apellido': 'Perez', 'sexo': 'M', 'idc':2, 'edad': 25})
db.alumnos.insert({'_id': 3, 'matricula':'25334455', 'nombre':'Luciana', 'apellido': 'Gonzalez', 'sexo': 'F', 'idc':3, 'edad': 27})
db.alumnos.insert({'_id': 4, 'matricula':'26334455', 'nombre':'Domenico', 'apellido': 'Pranzo', 'sexo': 'M', 'idc':1, 'edad': 19})
db.alumnos.insert({'_id': 5, 'matricula':'28334455', 'nombre':'Pedro', 'apellido': 'Sanchez', 'sexo': 'M', 'idc':2, 'edad': 28})
db.alumnos.insert({'_id': 6, 'matricula':'21314455', 'nombre':'Livia', 'apellido': 'Montalbano', 'sexo': 'F', 'idc':3, 'edad': 31})
db.alumnos.insert({'_id': 7, 'matricula':'22334456', 'nombre':'Jessica', 'apellido': 'Jones', 'sexo': 'F', 'idc':1, 'edad': 21})
db.alumnos.insert({'_id': 8, 'matricula':'22384457', 'nombre':'Olivia', 'apellido': 'Benson', 'sexo': 'F', 'idc':2, 'edad': 25})
db.alumnos.insert({'_id': 9, 'matricula':'21134452', 'nombre':'Harry', 'apellido': 'Callahan', 'sexo': 'M', 'idc':2, 'edad': 40})
db.alumnos.insert({'_id': 10, 'matricula':'22334468', 'nombre':'Roberto', 'apellido': 'Balboa', 'sexo': 'M', 'idc':3, 'edad': 23})
db.alumnos.insert({'_id': 11, 'matricula':'22331723', 'nombre':'Penelope', 'apellido': 'Glamour', 'sexo': 'F', 'idc':1, 'edad': 27})
db.alumnos.insert({'_id': 12, 'matricula':'22371999', 'nombre':'Juan', 'apellido': 'Salvo', 'sexo': 'M', 'idc':2, 'edad': 22})
