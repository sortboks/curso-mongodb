load('Dia 4/datos.js');

// Caunto pago por dia a cada sexo
db.empleados.aggregate(
  [
    {
      $match:{
        $or:[
          {sexo:'M'},
          {sexo:'F'}
        ]
      }
    },
    {
      $group:{
        _id:'$sexo',
        total:{$sum:'$salario'}
      }
    }
  ]
);

// Caunto pago por dia a las mujeres en cada area
db.empleados.aggregate(
  [
    {
      $match:{
          sexo:'F',
          edad:{ $gte: 20}
      }
    },
    {
      $group:{
        _id:'$area',
        total:{$sum:'$salario'}
      }
    }
  ]
);


// Caunto pago por dia en cada area
db.empleados.aggregate(
  [
    {
      $group:{
        _id:'$area',
        total:{$sum:'$salario'}
      }
    }
  ]
);

// Cuanto pague por mes y año 
db.empleados.aggregate(
  [
    {
      $group:{
        _id:{
          mes:{ $month:'$fechapago'},
          año:{ $year: '$fechapago'}
        },
        mensual:{$sum:{$multiply:['$dias','$salario']}}
      }
    }
  ]
);

// Cuanto pague por mes y año, el promedio trabajo 
db.empleados.aggregate(
  [
    {
      $group:{
        _id:{
          mes:{ $month:'$fechapago'},
          año:{ $year: '$fechapago'}
        },
        mensual:{$sum:{$multiply:['$dias','$salario']}},
        promediotrabajo: { $avg: '$dias'}
      }
    }
  ]
);

// Cuanto pague por mes y año, el promedio trabajo 
db.empleados.aggregate(
  [
    {
      $group:{
        _id:{
          mes:{ $month:'$fechapago'},
          año:{ $year: '$fechapago'}
        },
        mensual:{$sum:{$multiply:['$dias','$salario']}},
        promediotrabajo: { $avg: '$dias'},
        trabajaron: { $sum:1 }
      }
    }
  ]
);

// Proyeccion 
db.empleados.aggregate(
  [
    {
      $project: {
        nombre:1,
        salario:1
      }
    }
  ]
);

db.empleados.aggregate(
  [
    {
      $project: {
        identificacion:'$nombre',
        salario:1
      }
    }
  ]
);

// Verificacion presentismo
db.empleados.aggregate(
  [
    {
      $project: {
        _id:0,
        identificacion:'$nombre',
        salario:1,
        presentismo: {
          $cond:{
            if:{ $lte:['$dias',6]},
            then:'NO',
            else:'SI'
          }
        }
      }
    }
  ]
);



// Verificacion presentismo y el pago
db.empleados.aggregate(
  [
    {
      $project: {
        _id:0,
        identificacion:'$nombre',
        salario:1,
        presentismo: {
          $cond:{
            if:{ $lte:['$dias',6]},
            then:'NO',
            else:'SI'
          }
        },
        premio: {
          $cond:{
            if:{ $lte:['$dias',6]},
            then:{$multiply:['$dias',10]},
            else:{$multiply:['$dias',50]}
          }
        }
      }
    }
  ]
);

// Verificacion presentismo y el pago, ver la cantidad de premios y cuanto dinero
db.empleados.aggregate(
  [
    {
      $project: {
        _id:0,
        identificacion:'$nombre',
        salario:1,
        presentismo: {
          $cond:{
            if:{ $lte:['$dias',6]},
            then:'NO',
            else:'SI'
          }
        },
        premio: {
          $cond:{
            if:{ $lte:['$dias',6]},
            then:{$multiply:['$dias',10]},
            else:{$multiply:['$dias',50]}
          }
        }
      }
    },
    {
      $group:{
        _id: '$presentismo',
        subtotal:{ $sum: '$premio'}
      }
    },
    {
      $group:{
        _id:'',
        subtotal: { $sum:'$subtotal'}
      }
    }
  ]
);


// Union de 2 coleciones
db.alumnos.aggregate(
  [
    {
      $lookup:{
        from:'cursada',
        localField:'idc',
        foreignField: '_id',
        as:'cursada'
      }
    },
    {
      $match:{sexo:'F'}
    }
  ]
);



// Union de 2 coleciones
db.alumnos.aggregate(
  [
    {
      $lookup:{
        from:'cursada',
        localField:'idc',
        foreignField: '_id',
        as:'cursada'
      }
    },
    {
      $match:{
        sexo:'F',
        'cursada.carrera':'Sistemas'
      }
    },
    {
      $project:{
        _id:0,
        matricula:1,
        nombre:1,
        sexo:1,
        carrera:'$cursada.carrera'
      }
    }
  ]
);


// Union de 2 coleciones con merge
db.alumnos.aggregate(
  [
    {
      $lookup:{
        from:'cursada',
        localField:'idc',
        foreignField: '_id',
        as:'cursada'
      }
    },
    {
      $replaceRoot:{
        newRoot:{
          $mergeObjects:[
            {$arrayElemAt:['$cursada',0]},"$$ROOT"
          ]
        }
      }
    }
  ]
);

// Union de 2 coleciones con merge
db.alumnos.aggregate(
  [
    {
      $lookup:{
        from:'cursada',
        localField:'idc',
        foreignField: '_id',
        as:'cursada'
      }
    },
    {
      $replaceRoot:{
        newRoot:{
          $mergeObjects:[
            {$arrayElemAt:['$cursada',0]},"$$ROOT"
          ]
        }
      }
    },
    {
      $match:{
        edad:{ $gte:25},
        carrera:'Sistemas'
      }
    },
    {
      $sort:{
        apellido:1
      }
    },
    {
      $project:{
        _id:0,
        matricula:1,
        nombre: {
          $concat:['$nombre',' ','$apellido']
        },
        carrera:1,
        nombrec:1
      }
    }
  ]
);
