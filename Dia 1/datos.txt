user1 = {'username': 'user1','age': 41,'email': 'user1@mail.com'};
user2 = {'username': 'user2','age': 27,'email': 'user2@mail.com'};
user3 = {'username': 'user3','age': 19,'email': 'user3@mail.com'};
user4 = {'username': 'user4','age': 55,'email': 'user4@mail.com'};
user5 = {'username': 'user5','age': 46,'email': 'user5@mail.com'};
user6 = {'username': 'user6','age': 22,'email': 'user6@mail.com'};
user7 = {'username': 'user7','age': 25,'email': 'user7@mail.com'};
user8 = {'username': 'user8','age': 32,'email': 'user8@mail.com','status': 'inactive'};
user9 = {'username': 'user9','age': 31,'email': 'user9@mail.com','status': 'inactive'};
user10 = {'username': 'user10','age': 38,'email': 'user10@mail.com','status': 'inactive'};
user11 = {'username': 'user11','age': 42,'email': 'user11@mail.com','status': 'active'};
user12 = {'username': 'user12','age': 23,'email': 'user12@mail.com'};
user13 = {'username': 'user13','age': 36,'email': 'user13@mail.com'};
user14 = {'username': 'user14','age': 59,'email': 'user14@mail.com'};
user15 = {'username': 'user15','age': 44,'email': 'user15@mail.com'};
user16 = {'username': 'user16','age': 31,'email': 'user16@mail.com'};
user17 = {'username': 'user17','age': 52,'email': 'user17@mail.com'};
user18 = {'username': 'user18','age': 21,'email': 'user18@mail.com'};
user19 = {'username': 'user19','age': 18,'email': 'user19@mail.com'};
user20 = {'username': 'user20','age': 32,'email': 'user20@mail.com'};
user21 = {'username': 'user21','age': 43,'email': 'user21@mail.com'};
user22 = {'username': 'user22','age': 27,'email': 'user22@mail.com'};
user23 = {'username': 'user23','age': 23,'email': 'user23@mail.com'};
user24 = {'username': 'user24','age': 32,'email': 'user24@mail.com'};
user25 = {'username': 'user25','age': 19,'email': 'user25@mail.com'};
user26 = {'username': 'user26','age': 34,'email': 'user26@mail.com'};
user27 = {'username': 'user27','age': 45,'email': 'user27@mail.com'};
user28 = {'username': 'user28','age': 31,'email': 'user28@mail.com'};
user29 = {'username': 'user29','age': 51,'email': 'user29@mail.com'};
user30 = {'username': 'user30','age': 41,'email': 'user30@mail.com'};
user31 = {
  'username': 'user31',
  'email': 'user31@mail.com',
  'age': 29,
  'status': 'active',
  'address': {
    'zip': 1001,
    'country': 'AR'
  },
  'courses': ['Linux','KVM','Docker','Kubernetes'],
  'comments': [
    {
      'body': 'El mejor curso',
      'like': true,
      'tags': ['MongoDB']
    },
    {
      'body': 'Muy emocionado',
      'like': true
    },
    {
      'body': 'El curso esta OK'
    },
    {
      'body': 'Malos cursos, estoy muy disconforme',
      'like': false,
      'tags': ['malo','curso','MongoDB']
    }
  ]
};
user32 = {
  'username': 'user32',
  'email': 'user32@mail.com',
  'age': 31,
  'status': 'active',
  'comments': [
    {
      'body': 'El mejor curso',
      'like': true,
    }
  ]
};
user33 = {
  'username': 'user33',
  'email': 'user33@mail.com',
  'age': 27,
  'status': 'active',
  'comments': [
    {
      'body': 'El mejor curso',
      'like': false,
    }
  ]
}