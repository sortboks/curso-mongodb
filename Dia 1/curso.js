use curso;

show databases;

show collections;

user1 = {
  'username':'user1',
  'email': 'user1@mail.com',
  'age': 23
};

user2 = {
  'username': 'user2',
  'email': 'user2@mail.com',
  'age': 45
};

user3 = { 'username': 'user3', 'age': 19, 'email': 'user3@mail.com' };
user4 = { 'username': 'user4', 'age': 55, 'email': 'user4@mail.com' };
user5 = { 'username': 'user5', 'age': 46, 'email': 'user5@mail.com' };
user6 = { 'username': 'user6', 'age': 22, 'email': 'user6@mail.com' };


db.users.insertMany([user3, user4, user5, user6]);

db.users.insertMany([
  {
    'username': 'user7',
    'email': 'user7@mail',
    'age': 21
  },
  {
    'username': 'user70',
    'email': 'user70@mail',
    'age': 22
  }
]);

db.users.find({ username: 'user6' }); // Cursor

db.users.findOne({ username: 'user6' }); // Objecto

db.users.find({
  age: { $gt: 20}
});

db.users.find({
  age: { $lte: 30 }
});

// $gt Mayor
// $gte Mayor o igual
// $lt Menor
// $lte Menor o igual
// $ne no igual

db.users.find({
  age: 21
});

db.users.find().count();

db.users.find({
  age: { $lt: 30}
}).count();

db.users.find({
  age: { $ne: 22}
});

db.users.find({
  $or: [
    {age: 30},
    {age: 35},
    {age: 40},
    {age: 45},
    {username: 'user7'}
  ]
});

db.users.find({
  age: { $in: [30,35,40,45]}
});

db.users.find({
  $or: [
    {age: { $in: [30, 35, 40, 45] }},
    {username: 'user7' }
  ]
});

db.users.find({
  $or: [
    { age: { $in: [30, 35, 40, 45] } },
    { username: { $in: ['user7','user5'] }}
  ]
});

user8 = { 'username': 'user8', 'age': 32, 'email': 'user8@mail.com', 'status': 'inactive' };
user9 = { 'username': 'user9', 'age': 31, 'email': 'user9@mail.com', 'status': 'inactive' };
user10 = { 'username': 'user10', 'age': 38, 'email': 'user10@mail.com', 'status': 'inactive' };
user11 = { 'username': 'user11', 'age': 42, 'email': 'user11@mail.com', 'status': 'active' };

db.users.insertMany([user8, user9, user10, user11]);

db.users.find();

db.users.find({
  status: { $exists: true}
});


db.users.find({
  status: { $exists: true }
}).count();


db.users.find({
  status: { $exists: false }
}).count();

db.users.find({
  $and: [
    { status: { $exists: true}},
    { status: 'active'}
  ]
}).count();

db.users.find({
  $and: [
    { status: { $exists: true } },
    { status: 'inactive' }
  ]
}).count();

db.users.find({
  $and: [
    { status: { $exists: true } },
    { status: 'inactive' },
    { address: { $exists: true}}
  ]
}).count();