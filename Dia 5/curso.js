var lista=db.alumnos.find().toArray();
lista
console.table(lista);
fs.writeFileSync('./Dia 5/lista.json',JSON.stringify(lista,null,2));

db.alumnos.createIndex.help();
db.alumnos.createIndex({matricula:1});
snippet 
snippet help
snippet search
snippet install analyze-schema
snippet ls 
schema(db.alumnos);
db.alumnos.insertOne({
  'apellido':'Alberto',
  'nombre':'Perez',
  'edad': '45',
  'idc': 1,
  'matricula': 12345679,
  'sexo': 'F'
});

db.alumnos.aggregate(
  [
    {
      $lookup:{
        from:'cursada',
        localField:'idc',
        foreignField: '_id',
        as:'cursada'
      }
    },
    {
      $replaceRoot:{
        newRoot:{
          $mergeObjects:[
            {$arrayElemAt:['$cursada',0]},"$$ROOT"
          ]
        }
      }
    },
    {
      $match:{
        edad:{ $gte:25},
        carrera:'Sistemas'
      }
    },
    {
      $sort:{
        apellido:1
      }
    },
    {
      $project:{
        _id:0,
        matricula:1,
        nombre: {
          $concat:['$nombre',' ','$apellido']
        },
        carrera:1,
        nombrec:1
      }
    },
    {
      $out: 'salida'
    }
  ]
);


session=db.getMongo().startSession();
session.startTransaction();
session.getDatabase("curso").alumnos.insertOne({
  'apellido':'Henry',
  'nombre':'Terry',
  'edad': 45,
  'idc': 1,
  'matricula': '12738465',
  'sexo': 'M'
});

session.commitTransaction();
session.endSession();


use ferretuti;



session=db.getMongo().startSession();
session.startTransaction();
session.getDatabase("curso").movimientos.insertOne({
  'codigoarcticulo':'mb4',
  'cantidad': 1,
  'fecha': new Date(),
  'tipo': 'venta'
})

session.getDatabase("curso").articulos.updateOne(
  {
    codigo: 'mb4'
  },
  {
    $inc: {
      cantidad: -1
    }
  }
);
session.commitTransaction();
session.endSession();


function convertStoI() {
  var a = "100";
  var b = parseInt(a);
  document.write("Integer value is" + b);
  var d = parseInt("3 11 43");
  document.write("</br>");
  document.write('Integer value is ' + d);
}