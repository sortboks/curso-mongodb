function ventas(art,cant){
  session=db.getMongo().startSession();
  session.startTransaction();
  session.getDatabase("ferretuti").movimientos.insertOne({
    'codigoarcticulo':art,
    'cantidad': cant,
    'fecha': new Date(),
    'tipo': 'venta'
  })

  session.getDatabase("ferretuti").articulos.updateOne(
    {
      codigo: art
    },
    {
      $inc: {
        cantidad: cant*-1
      }
    }
  );
  session.commitTransaction();
  session.endSession();
};

function compra(art,cant,provee){
  session=db.getMongo().startSession();
  session.startTransaction();
  session.getDatabase("ferretuti").movimientos.insertOne({
    'codigoarcticulo':art,
    'cantidad': cant,
    'fecha': new Date(),
    'tipo': 'compra',
    'proveedor': provee
  })

  session.getDatabase("ferretuti").articulos.updateOne(
    {
      codigo: art
    },
    {
      $inc: {
        cantidad: cant
      },
      $set:{
        proveedor: provee
      }
    }
  );
  session.commitTransaction();
  session.endSession();
};
