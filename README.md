# Curso basico de MongoDB #



### ¿Para qué es este repositorio? ###

* Este repositorio es utilizado durante el curso basico de MongoDB
* Version 1.0
* [Aprender Markdown](https://bitbucket.org/tutorials/markdowndemo)

### ¿Cómo lo configuro? ###

* Creo un directorio de trabajo
* Me posiciono en el directorio de trabajo
* Ejecuto: git clone https://msaporito@bitbucket.org/sortboks/curso-mongodb.git

### Links utiles ###
* [Docker](http://hub.docker.com)

* [MongoDB](http://www.mongodb.com)

* [Documentacion MongoDB](https://docs.mongodb.com/manual/)

* [Robo3T](https://robomongo.org/)

* [Mongo shell](https://docs.mongodb.com/mongodb-shell/)

* [Repositorio Curso](https://msaporito@bitbucket.org/sortboks/curso-mongodb.git)

* [Introducción a JSON](https://www.json.org/json-es.html)